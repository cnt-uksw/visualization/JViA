/*
 * Java Visualization Algorithms (JViA)
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jvia.spatialops;

import java.util.ArrayList;
import org.visnow.jlargearrays.ComplexDoubleLargeArray;
import org.visnow.jlargearrays.ComplexFloatLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jlargearrays.LargeArrayType;
import org.visnow.jlargearrays.LargeArrayUtils;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArraySchema;
import org.visnow.jscic.utils.VectorMath;

/**
 * Padding operations.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class Padding
{

    private Padding()
    {
    }

    /**
     * Types of padding.
     */
    public enum PaddingType
    {

        ZERO("Zero"),
        FIXED("Fixed"),
        PERIODIC("Periodic"),
        REFLECTED("Reflected");

        private final String name;

        private PaddingType(String name)
        {
            this.name = name;
        }

        @Override
        public String toString()
        {
            return name;
        }
    }

    /**
     * Pads selected components of RegularField <code>inField</code> by <code>paddingSize</code> size.
     * New field has dimensions <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>.
     *
     * @param inField            input field. RegularFields with explicit coordinates are not supported.
     * @param selectedComponents indices of components in the input field that will be padded and added to the result. If null or empty, then all components are
     *                           padded.
     * @param paddingSize        the size of padding in each dimension: <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>. 
     * @param paddingType        if <code>PaddingType.ZERO</code>, border is filled by zeros, if <code>PaddingType.FIXED</code>, border is filled by nearest
     *                           values of original component, if <code>PaddingType.PERIODIC</code>, border is filled by periodic extension,
     *                           if <code>PaddingType.REFLECTED</code>, border is filled by reflected extension.
     * @param reflectVectors     if true, then vectors are reflected when <code>paddingType = PaddingType.REFLECTED</code>.
     *
     * @return Padded field of dimensions  <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>.
     */
    public static RegularField padField(final RegularField inField, final int[] selectedComponents, final long[][] paddingSize, final PaddingType paddingType, final boolean reflectVectors)
    {
        if (inField == null || paddingSize == null || paddingType == null) {
            throw new IllegalArgumentException("Null arguments are not supported.");
        }
        inField.checkTrueNSpace();
        if (inField.getCoords() != null) {
            throw new IllegalArgumentException("RegularFields with explicit coordinates are not supported.");
        }
        int[] componentIDs;
        if (selectedComponents == null || selectedComponents.length == 0) {
            componentIDs = new int[inField.getNComponents()];
            for (int i = 0; i < componentIDs.length; i++) {
                componentIDs[i] = i;
            }
        } else {
            componentIDs = selectedComponents;
        }
        long[] dims = inField.getLDims();
        checkPaddingSize(paddingSize);
        long[] outDims = computeOutDims(dims, paddingSize);

        RegularField outField = new RegularField(outDims);
        if (inField.getAxesNames() != null) {
            outField.setAxesNames(inField.getAxesNames().clone());
        }
        if (inField.getCoordsUnits() != null) {
            outField.setCoordsUnits(inField.getCoordsUnits().clone());
        }
        outField.setName(inField.getName());
        outField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        outField.setTimeUnit(inField.getTimeUnit());
        if (inField.getUserData() != null) {
            outField.setUserData(inField.getUserData().clone());
        }
        if (inField.hasMask()) {
            outField.setMask(padTimeData(inField.getMask(), dims, 1, paddingSize, paddingType, reflectVectors));
        }

        float[][] inAffine = inField.getAffine();
        float[][] outAffine = new float[4][3];
        for (int i = 0; i < inAffine.length; i++) {
            System.arraycopy(inAffine[i], 0, outAffine[i], 0, 3);
        }

        if (dims.length == 1 && outDims.length > 1) {
            outAffine[1] = VectorMath.vectorPerpendicular(inAffine[0], false);
            outAffine[1] = VectorMath.vectorMultiplyScalar(VectorMath.vectorNormalize(outAffine[1], false), VectorMath.vectorNorm(outAffine[0]), false);
        }

        if (dims.length <= 2 && outDims.length == 3) {
            outAffine[2] = VectorMath.crossProduct(outAffine[0], outAffine[1]);
            outAffine[2] = VectorMath.vectorMultiplyScalar(VectorMath.vectorNormalize(outAffine[2], false), VectorMath.vectorNorm(outAffine[0]), false);
        }

        for (int vectorIndex = 0; vectorIndex < outDims.length; vectorIndex++) {
            for (int i = 0; i < 3; i++) {
                outAffine[3][i] -= paddingSize[vectorIndex][0] * outAffine[vectorIndex][i];
            }
        }
        outField.setAffine(outAffine);
        outField.setPreferredExtents(outField.getExtents()); //  reset preferred extents 

        for (int n = 0; n < componentIDs.length; n++) {
            outField.addComponent(padDataArray(inField.getComponent(componentIDs[n]), dims, paddingSize, paddingType, reflectVectors));
        }
        return outField;
    }

    /**
     * Pads array <code>inArray</code> (regarded as array of dimensions <code>inDims</code>) by <code>paddingSize</code> size.
     * New array has dimensions <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>.
     *
     * @param inArray        input array of length <code>vlen * inDims[0] * ...</code>.
     * @param inDims         dimensions of the input array.
     * @param paddingSize    the size of padding in each dimension: <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>. 
     * @param paddingType    if <code>PaddingType.ZERO</code>, border is filled by zeros, if <code>PaddingType.FIXED</code>, border is filled by nearest
     *                       values of original component, if <code>PaddingType.PERIODIC</code>, border is filled by periodic extension,
     *                       if <code>PaddingType.REFLECTED</code>, border is filled by reflected extension.
     * @param reflectVectors if true, then vectors are reflected when <code>paddingType = PaddingType.REFLECTED</code>.
     *
     * @return padded array of length <code>vlen * outDims[0] * ...</code>.
     */
    public static DataArray padDataArray(final DataArray inArray, final long[] inDims, final long[][] paddingSize, final PaddingType paddingType, final boolean reflectVectors)
    {
        if (inArray == null || inDims == null || paddingSize == null || paddingType == null) {
            throw new IllegalArgumentException("Null arguments are not supported.");
        }
        checkDimensions(inDims, inDims.length);
        checkPaddingSize(paddingSize);

        TimeData timeData = padTimeData(inArray.getTimeData(), inDims, inArray.getVectorLength(), paddingSize, paddingType, reflectVectors);
        DataArraySchema schema = inArray.getSchema().cloneDeep();
        schema.setNElements(timeData.getCurrentValue().length() / inArray.getVectorLength());
        DataArray da = DataArray.create(schema);
        da.setTimeData(timeData);
        da.getSchema().setStatisticsComputed(false);
        return da;
    }

    /**
     * Pads time data <code>tdata</code> (regarded as array of dimensions <code>inDims</code>) by <code>paddingSize</code> size.
     * New time data has dimensions <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>.
     *
     * @param tdata          input time data of length <code>vlen * inDims[0] * ...</code>.
     * @param inDims         dimensions of the input time data.
     * @param vlen           length of vectors elements of the input time data.
     * @param paddingSize    the size of padding in each dimension: <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>. 
     * @param paddingType    if <code>PaddingType.ZERO</code>, border is filled by zeros, if <code>PaddingType.FIXED</code>, border is filled by nearest
     *                       values of original component, if <code>PaddingType.PERIODIC</code>, border is filled by periodic extension,
     *                       if <code>PaddingType.REFLECTED</code>, border is filled by reflected extension.
     * @param reflectVectors if true, then vectors are reflected when <code>paddingType = PaddingType.REFLECTED</code>.
     *
     * @return padded time data of length <code>vlen * outDims[0] * ...</code>.
     */
    public static TimeData padTimeData(final TimeData tdata, final long[] inDims, final int vlen, final long[][] paddingSize, final PaddingType paddingType, final boolean reflectVectors)
    {
        if (tdata == null || inDims == null || paddingSize == null || paddingType == null) {
            throw new IllegalArgumentException("Null arguments are not supported.");
        }
        checkDimensions(inDims, inDims.length);
        if (vlen < 1) {
            throw new IllegalArgumentException("Invalid vector length.");
        }
        checkPaddingSize(paddingSize);
  
        ArrayList<Float> times = tdata.getTimesAsList();
        ArrayList<LargeArray> timeValues = new ArrayList<>(times.size());
        times.forEach((time) -> {
            timeValues.add(padLargeArray(tdata.getValue(time), inDims, vlen, paddingSize, paddingType, reflectVectors));
        });
        TimeData timeData = new TimeData(times, timeValues, tdata.getCurrentTime());
        timeData.setNanAction(tdata.getNanAction());
        timeData.setInfinityAction(tdata.getInfinityAction());
        if (tdata.getUserData() != null) {
            ArrayList<String[]> userDataClone = new ArrayList<>(tdata.getUserData().size());
            for (String[] element : tdata.getUserData()) {
                if (element != null) {
                    userDataClone.add(element.clone());
                } else {
                    userDataClone.add(null);
                }
            }
            timeData.setUserData(userDataClone);
        }
        return timeData;
    }

    /**
     * Pads array <code>inArray</code> (regarded as array of dimensions <code>inDims</code>) by <code>paddingSize</code> size.
     * New array has dimensions <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>.
     *
     * @param inArray        input array of length <code>vlen * inDims[0] * ...</code>.
     * @param inDims         dimensions of the input array.
     * @param vlen           length of vectors elements of the input array.
     * @param paddingSize    the size of padding in each dimension: <code>outDims[i] = paddingSize[i][0] + inDims[i] + paddingSize[i][1]</code>. 
     * @param paddingType    if <code>PaddingType.ZERO</code>, border is filled by zeros, if <code>PaddingType.FIXED</code>, border is filled by nearest
     *                       values of original component, if <code>PaddingType.PERIODIC</code>, border is filled by periodic extension,
     *                       if <code>PaddingType.REFLECTED</code>, border is filled by reflected extension.
     * @param reflectVectors if true, then vectors are reflected when <code>paddingType = PaddingType.REFLECTED</code>.
     *
     * @return padded array of length <code>vlen * outDims[0] * ...</code>.
     */
    public static LargeArray padLargeArray(final LargeArray inArray, final long[] inDims, final int vlen, final long[][] paddingSize, final PaddingType paddingType, final boolean reflectVectors)
    {
        if (inArray == null || inDims == null || paddingSize == null || paddingType == null) {
            throw new IllegalArgumentException("Null arguments are not supported.");
        }

        checkDimensions(inDims, inDims.length);
        if (vlen < 1) {
            throw new IllegalArgumentException("Invalid vector length.");
        }
        checkPaddingSize(paddingSize);
        long[] outDims = computeOutDims(inDims, paddingSize);

        long nExt = 1;
        for (int i = 0; i < outDims.length; i++) {
            nExt *= outDims[i];
        }

        LargeArray outArray = null;

        if (inArray.isConstant()) {
            if (paddingType == PaddingType.ZERO) {
                if ((inArray.getType().isRealNumericType() && inArray.getDouble(0) == 0.0) ||
                    (inArray.getType() == LargeArrayType.STRING && inArray.get(0).equals("")) ||
                    (inArray.getType() == LargeArrayType.OBJECT && inArray.get(0) == null)) {
                    outArray = LargeArrayUtils.createConstant(inArray.getType(), vlen * nExt, inArray.get(0));
                } else if (inArray.getType() == LargeArrayType.COMPLEX_FLOAT) {
                    ComplexFloatLargeArray cla = (ComplexFloatLargeArray) inArray;
                    float[] elem = cla.getComplexFloat(0);
                    if (elem[0] == 0.0f && elem[1] == 0.0f) {
                        outArray = LargeArrayUtils.createConstant(inArray.getType(), vlen * nExt, elem);
                    }
                } else if (inArray.getType() == LargeArrayType.COMPLEX_DOUBLE) {
                    ComplexDoubleLargeArray cla = (ComplexDoubleLargeArray) inArray;
                    double[] elem = cla.getComplexDouble(0);
                    if (elem[0] == 0.0 && elem[1] == 0.0) {
                        outArray = LargeArrayUtils.createConstant(inArray.getType(), vlen * nExt, elem);
                    }
                }
            } else {
                outArray = LargeArrayUtils.createConstant(inArray.getType(), vlen * nExt, inArray.get(0));
            }
        }
        if (outArray != null) {
            return outArray;
        }

        if (paddingType == PaddingType.ZERO) {
            outArray = LargeArrayUtils.create(inArray.getType(), vlen * nExt);
        } else {
            outArray = LargeArrayUtils.create(inArray.getType(), vlen * nExt, false);
        }
        long offset = 0;

        switch (inDims.length) {
            case 3:
                switch (paddingType) {
                    case ZERO:
                        for (long i = 0, m = 0; i < inDims[2]; i++) {
                            for (long j = 0; j < inDims[1]; j++) {
                                for (long k = 0, l = ((paddingSize[2][0] + i) * outDims[1] + paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0]; k < inDims[0]; k++, l++, m++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l * vlen + n, inArray.get(m * vlen + n));
                                    }
                                }
                            }
                        }
                        break;
                    case FIXED:
                        for (long i = 0, m = 0; i < inDims[2]; i++) {
                            for (long j = 0; j < inDims[1]; j++) {
                                for (long k = 0, l = ((paddingSize[2][0] + i) * outDims[1] + paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0]; k < inDims[0]; k++, l++, m++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l * vlen + n, inArray.get(m * vlen + n));
                                    }
                                }
                            }
                        }
                        long l = 0;
                        for (long i = 0; i < inDims[2]; i++) {
                            for (long j = 0; j < inDims[1]; j++) {
                                l = (((paddingSize[2][0] + i) * outDims[1] + paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0]) * vlen;
                                for (long k = 1; k <= paddingSize[0][0]; k++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l - k * vlen + n, outArray.get(l + n));
                                    }
                                }
                                l = (((paddingSize[2][0] + i) * outDims[1] + paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0] + inDims[0] - 1) * vlen;
                                for (long k = 1; k <= paddingSize[0][1]; k++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l + k * vlen + n, outArray.get(l + n));
                                    }
                                }
                            }
                        }
                        long m = outDims[0];
                        for (long i = 0; i < inDims[2]; i++) {
                            for (long j = 0; j < outDims[0]; j++) {
                                l = (((paddingSize[2][0] + i) * outDims[1] + paddingSize[1][0]) * outDims[0] + j) * vlen;
                                for (long k = 1; k <= paddingSize[1][0]; k++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l - k * m * vlen + n, outArray.get(l + n));
                                    }
                                }
                                l = (((paddingSize[2][0] + i) * outDims[1] + paddingSize[1][0] + inDims[1] - 1) * outDims[0] + j) * vlen;
                                for (long k = 1; k <= paddingSize[1][1]; k++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l + k * m * vlen + n, outArray.get(l + n));
                                    }
                                }
                            }
                        }
                        m = outDims[1] * outDims[0];
                        for (long i = 0; i < outDims[1]; i++) {
                            for (long j = 0; j < outDims[0]; j++) {
                                l = ((paddingSize[2][0] * outDims[1] + i) * outDims[0] + j) * vlen;
                                for (long k = 1; k <= paddingSize[2][0]; k++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l - k * m * vlen + n, outArray.get(l + n));
                                    }
                                }
                                l = (((paddingSize[2][0] + inDims[2] - 1) * outDims[1] + i) * outDims[0] + j) * vlen;
                                for (long k = 1; k <= paddingSize[2][1]; k++) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(l + k * m * vlen + n, outArray.get(l + n));
                                    }
                                }
                            }
                        }
                        break;
                    case PERIODIC: {
                        long sOff = paddingSize[2][0];
                        long rOff = paddingSize[1][0];
                        long cOff = paddingSize[0][0];
                        for (long s = -sOff; s < outDims[2] - sOff; s++) {
                            long sOut = s + sOff;
                            long sIn = periodic(s, inDims[2]);
                            for (long r = -rOff; r < outDims[1] - rOff; r++) {
                                long rOut = r + rOff;
                                long rIn = periodic(r, inDims[1]);
                                for (long c = -cOff; c < outDims[0] - cOff; c++) {
                                    long cOut = c + cOff;
                                    long cIn = periodic(c, inDims[0]);
                                    long idx = (sIn * inDims[1] * inDims[0] + rIn * inDims[0] + cIn) * vlen;
                                    long idxExt = (sOut * outDims[1] * outDims[0] + rOut * outDims[0] + cOut) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxExt + n, inArray.get(idx + n));
                                    }
                                }
                            }
                        }
                        break;
                    }
                    case REFLECTED: {
                        long sOff = paddingSize[2][0];
                        long rOff = paddingSize[1][0];
                        long cOff = paddingSize[0][0];
                        for (long s = -sOff; s < outDims[2] - sOff; s++) {
                            long sOut = s + sOff;
                            long sIn = reflected(s, inDims[2]);
                            for (long r = -rOff; r < outDims[1] - rOff; r++) {
                                long rOut = r + rOff;
                                long rIn = reflected(r, inDims[1]);
                                for (long c = -cOff; c < outDims[0] - cOff; c++) {
                                    long cOut = c + cOff;
                                    long cIn = reflected(c, inDims[0]);
                                    long idx = (sIn * inDims[1] * inDims[0] + rIn * inDims[0] + cIn) * vlen;
                                    long idxExt = (sOut * outDims[1] * outDims[0] + rOut * outDims[0] + cOut) * vlen;
                                    if (reflectVectors == false || vlen == 1 || inArray.getType() == LargeArrayType.LOGIC || inArray.getType() == LargeArrayType.COMPLEX_FLOAT || inArray.getType() == LargeArrayType.COMPLEX_DOUBLE || inArray.getType() == LargeArrayType.STRING || inArray.getType() == LargeArrayType.OBJECT) {
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxExt + n, inArray.get(idx + n));
                                        }
                                    } else {
                                        if (vlen >= 3) {
                                            if (sIn + sOff != sOut) {
                                                outArray.setDouble(idxExt + 2, -inArray.getDouble(idx + 2));
                                            } else {
                                                outArray.set(idxExt + 2, inArray.get(idx + 2));
                                            }
                                        }
                                        if (vlen >= 2) {
                                            if (rIn + rOff != rOut) {
                                                outArray.setDouble(idxExt + 1, -inArray.getDouble(idx + 1));
                                            } else {
                                                outArray.set(idxExt + 1, inArray.get(idx + 1));
                                            }
                                        }
                                        if (cIn + cOff != cOut) {
                                            outArray.setDouble(idxExt, -inArray.getDouble(idx));
                                        } else {
                                            outArray.set(idxExt, inArray.get(idx));
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    }
                    default:
                        throw new IllegalArgumentException("Invalid padding type.");
                }
                break;
            case 2:
                if (outDims.length == 3) {
                    offset = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                }
                switch (paddingType) {
                    case ZERO:
                        for (long j = 0, m = 0; j < inDims[1]; j++) {
                            for (long k = 0, l = (paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0]; k < inDims[0]; k++, l++, m++) {
                                for (long n = 0; n < vlen; n++) {
                                    outArray.set(offset + l * vlen + n, inArray.get(m * vlen + n));
                                }
                            }
                        }
                        break;
                    case FIXED:
                        for (long j = 0, m = 0; j < inDims[1]; j++) {
                            for (long k = 0, l = (paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0]; k < inDims[0]; k++, l++, m++) {
                                for (long n = 0; n < vlen; n++) {
                                    outArray.set(offset + l * vlen + n, inArray.get(m * vlen + n));
                                }
                            }
                        }
                        long l = 0;
                        for (long j = 0; j < inDims[1]; j++) {
                            l = ((paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0]) * vlen;
                            for (long k = 1; k <= paddingSize[0][0]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    outArray.set(offset + l - k * vlen + n, outArray.get(offset + l + n));
                                }
                            }
                            l = ((paddingSize[1][0] + j) * outDims[0] + paddingSize[0][0] + inDims[0] - 1) * vlen;
                            for (long k = 1; k <= paddingSize[0][1]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    outArray.set(offset + l + k * vlen + n, outArray.get(offset + l + n));
                                }
                            }
                        }
                        long m = outDims[0];
                        for (long j = 0; j < outDims[0]; j++) {
                            l = ((paddingSize[1][0]) * outDims[0] + j) * vlen;
                            for (long k = 1; k <= paddingSize[1][0]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    outArray.set(offset + l - k * m * vlen + n, outArray.get(offset + l + n));
                                }
                            }
                            l = ((paddingSize[1][0] + inDims[1] - 1) * outDims[0] + j) * vlen;
                            for (long k = 1; k <= paddingSize[1][1]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    outArray.set(offset + l + k * m * vlen + n, outArray.get(offset + l + n));
                                }
                            }

                            if (outDims.length == 3) {
                                for (long s = 0; s < paddingSize[2][0]; s++) {
                                    long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                    for (long r = 0; r < outDims[1]; r++) {
                                        for (long c = 0; c < outDims[0]; c++) {
                                            long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                            for (long n = 0; n < vlen; n++) {
                                                outArray.set(idxOut + n, outArray.get(idx + n));
                                            }
                                            idx += vlen;
                                        }
                                    }
                                }
                                for (long s = paddingSize[2][0] + 1; s < outDims[2]; s++) {
                                    long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                    for (long r = 0; r < outDims[1]; r++) {
                                        for (long c = 0; c < outDims[0]; c++) {
                                            long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                            for (long n = 0; n < vlen; n++) {
                                                outArray.set(idxOut + n, outArray.get(idx + n));
                                            }
                                            idx += vlen;
                                        }
                                    }
                                }
                            }

                        }
                        break;
                    case PERIODIC: {
                        long rOff = paddingSize[1][0];
                        long cOff = paddingSize[0][0];
                        for (long r = -rOff; r < outDims[1] - rOff; r++) {
                            long rOut = r + rOff;
                            long rIn = periodic(r, inDims[1]);
                            for (long c = -cOff; c < outDims[0] - cOff; c++) {
                                long cOut = c + cOff;
                                long cIn = periodic(c, inDims[0]);
                                long idx = (rIn * inDims[0] + cIn) * vlen;
                                long idxExt = (rOut * outDims[0] + cOut) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    outArray.set(offset + idxExt + n, inArray.get(idx + n));
                                }
                            }
                        }
                        if (outDims.length == 3) {
                            for (long s = 0; s < paddingSize[2][0]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                            for (long s = paddingSize[2][0] + 1; s < outDims[2]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    case REFLECTED: {
                        long rOff = paddingSize[1][0];
                        long cOff = paddingSize[0][0];
                        for (long r = -rOff; r < outDims[1] - rOff; r++) {
                            long rOut = r + rOff;
                            long rIn = reflected(r, inDims[1]);
                            for (long c = -cOff; c < outDims[0] - cOff; c++) {
                                long cOut = c + cOff;
                                long cIn = reflected(c, inDims[0]);
                                long idx = (rIn * inDims[0] + cIn) * vlen;
                                long idxExt = (rOut * outDims[0] + cOut) * vlen;
                                if (reflectVectors == false || vlen == 1 || inArray.getType() == LargeArrayType.LOGIC || inArray.getType() == LargeArrayType.COMPLEX_FLOAT || inArray.getType() == LargeArrayType.COMPLEX_DOUBLE || inArray.getType() == LargeArrayType.STRING || inArray.getType() == LargeArrayType.OBJECT) {
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(offset + idxExt + n, inArray.get(idx + n));
                                    }
                                } else {
                                    if (vlen >= 2) {
                                        if (rIn + rOff != rOut) {
                                            outArray.setDouble(idxExt + 1, -inArray.getDouble(idx + 1));
                                        } else {
                                            outArray.set(idxExt + 1, inArray.get(idx + 1));
                                        }
                                    }
                                    if (cIn + cOff != cOut) {
                                        outArray.setDouble(idxExt, -inArray.getDouble(idx));
                                    } else {
                                        outArray.set(idxExt, inArray.get(idx));
                                    }
                                }
                            }
                        }
                        if (outDims.length == 3) {
                            for (long s = 0; s < paddingSize[2][0]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                            for (long s = paddingSize[2][0] + 1; s < outDims[2]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    default:
                        throw new IllegalArgumentException("Invalid padding type.");
                }
                break;
            case 1:
                if (outDims.length == 2) {
                    offset = paddingSize[1][0] * outDims[0] * vlen;
                } else if (outDims.length == 3) {
                    offset = paddingSize[2][0] * outDims[1] * outDims[0] * vlen + paddingSize[1][0] * outDims[0] * vlen;
                }
                switch (paddingType) {
                    case ZERO:
                        for (long k = 0, m = 0, l = paddingSize[0][0]; k < inDims[0]; k++, l++, m++) {
                            for (long n = 0; n < vlen; n++) {
                                outArray.set(offset + l * vlen + n, inArray.get(m * vlen + n));
                            }
                        }
                        break;
                    case FIXED:
                        for (long k = 0, m = 0, l = paddingSize[0][0]; k < inDims[0]; k++, l++, m++) {
                            for (long n = 0; n < vlen; n++) {
                                outArray.set(offset + l * vlen + n, inArray.get(m * vlen + n));
                            }
                        }
                        long l = (paddingSize[0][0]) * vlen;
                        for (long k = 1; k <= paddingSize[0][0]; k++) {
                            for (long n = 0; n < vlen; n++) {
                                outArray.set(offset + l - k * vlen + n, outArray.get(offset + l + n));
                            }
                        }
                        l = (paddingSize[0][0] + inDims[0] - 1) * vlen;
                        for (long k = 1; k <= paddingSize[0][1]; k++) {
                            for (long n = 0; n < vlen; n++) {
                                outArray.set(offset + l + k * vlen + n, outArray.get(offset + l + n));
                            }
                        }
                        if (outDims.length == 2) {
                            for (long r = 0; r < paddingSize[1][0]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long r = paddingSize[1][0] + 1; r < outDims[1]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                        } else if (outDims.length == 3) {
                            for (long r = 0; r < paddingSize[1][0]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long r = paddingSize[1][0] + 1; r < outDims[1]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long s = 0; s < paddingSize[2][0]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }

                            for (long s = paddingSize[2][0] + 1; s < outDims[2]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                        }
                        break;
                    case PERIODIC: {
                        long cOff = paddingSize[0][0];
                        for (long c = -cOff; c < outDims[0] - cOff; c++) {
                            long cOut = c + cOff;
                            long cIn = periodic(c, inDims[0]);
                            long idx = cIn * vlen;
                            long idxExt = cOut * vlen;
                            for (long n = 0; n < vlen; n++) {
                                outArray.set(offset + idxExt + n, inArray.get(idx + n));
                            }
                        }
                        if (outDims.length == 2) {
                            for (long r = 0; r < paddingSize[1][0]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long r = paddingSize[1][0] + 1; r < outDims[1]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                        } else if (outDims.length == 3) {
                            for (long r = 0; r < paddingSize[1][0]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long r = paddingSize[1][0] + 1; r < outDims[1]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long s = 0; s < paddingSize[2][0]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                            for (long s = paddingSize[2][0] + 1; s < outDims[2]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    case REFLECTED: {
                        long cOff = paddingSize[0][0];
                        for (long c = -cOff; c < outDims[0] - cOff; c++) {
                            long cOut = c + cOff;
                            long cIn = reflected(c, inDims[0]);
                            long idx = cIn * vlen;
                            long idxExt = cOut * vlen;
                            for (long n = 0; n < vlen; n++) {
                                outArray.set(offset + idxExt + n, inArray.get(idx + n));
                            }
                        }
                        if (outDims.length == 2) {
                            for (long r = 0; r < paddingSize[1][0]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long r = paddingSize[1][0] + 1; r < outDims[1]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                        } else if (outDims.length == 3) {
                            for (long r = 0; r < paddingSize[1][0]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long r = paddingSize[1][0] + 1; r < outDims[1]; r++) {
                                long idx = paddingSize[1][0] * outDims[0] * vlen;
                                for (long c = 0; c < outDims[0]; c++) {
                                    long idxOut = (r * outDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        outArray.set(idxOut + n, outArray.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }

                            for (long s = 0; s < paddingSize[2][0]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                            for (long s = paddingSize[2][0] + 1; s < outDims[2]; s++) {
                                long idx = paddingSize[2][0] * outDims[1] * outDims[0] * vlen;
                                for (long r = 0; r < outDims[1]; r++) {
                                    for (long c = 0; c < outDims[0]; c++) {
                                        long idxOut = (s * outDims[1] * outDims[0] + r * outDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            outArray.set(idxOut + n, outArray.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                        }
                        break;
                    }
                    default:
                        throw new IllegalArgumentException("Invalid padding type.");
                }
                break;
        }
        return outArray;
    }

    private static long reflected(final long i, final long n)
    {
        long ip = mod(i, 2 * n);
        if (ip < n) {
            return ip;
        } else {
            return n - (ip % n) - 1;
        }
    }

    private static long mod(final long i, final long n)
    {
        return ((i % n) + n) % n;
    }

    private static long periodic(final long i, final long n)
    {
        long ip = mod(i, 2 * n);
        if (ip < n) {
            return ip;
        } else {
            return (ip % n);
        }
    }

    private static long[] computeOutDims(long[] inDims, long[][] margins)
    {
        long[] out = {1, 1, 1};
        int space = inDims.length;
        for (int i = 0; i < margins.length; i++) {
            long[] is = margins[i];
            if (is[0] > 0 || is[1] > 0) {
                if (i < inDims.length) {
                    out[i] = inDims[i] + is[0] + is[1];
                } else {
                    out[i] = is[0] + is[1] + 1;
                }
            } else if (i < inDims.length) {
                out[i] = inDims[i];
            }
        }
        for (int i = 2; i >= 0; i--) {
            if (out[i] > 1) {
                space = i + 1;
                break;
            }
        }
        long[] outDims = new long[space];
        System.arraycopy(out, 0, outDims, 0, space);
        return outDims;
    }

    private static void checkPaddingSize(final long[][] paddingSize)
    {
        if (paddingSize == null || paddingSize.length < 1 || paddingSize.length > 3) {
            throw new IllegalArgumentException("Invalid padding size.");
        }
        for (int i = 0; i < paddingSize.length; i++) {
            if (paddingSize[i] == null || paddingSize[i].length != 2 || paddingSize[i][0] < 0 || paddingSize[i][1] < 0) {
                throw new IllegalArgumentException("Invalid padding size.");
            }
        }
    }

    private static void checkDimensions(final long[] dims, final int rank)
    {
        if (dims == null || dims.length != rank) {
            throw new IllegalArgumentException("Invalid dimensions.");
        }
    }

}
