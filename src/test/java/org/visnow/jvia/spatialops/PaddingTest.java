/*
 * Java Visualization Algorithms (JViA)
 * Copyright (C) 2020 onward visnow.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 */
package org.visnow.jvia.spatialops;

import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.visnow.jlargearrays.FloatingPointAsserts;
import org.visnow.jlargearrays.IntLargeArray;
import org.visnow.jlargearrays.LargeArray;
import org.visnow.jscic.RegularField;
import org.visnow.jscic.TimeData;
import org.visnow.jscic.dataarrays.DataArray;
import org.visnow.jscic.dataarrays.DataArrayType;

/**
 * Tests of padding operations.
 *
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class PaddingTest
{

    private static final int[] DATA = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};

    private static final int[] PADED_1D_ZERO = new int[]{0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 0, 0};
    private static final int[] PADED_1D_FIXED = new int[]{1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 24, 24, 24};
    private static final int[] PADED_1D_PERIODIC = new int[]{23, 24, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 1, 2, 3};
    private static final int[] PADED_1D_REFLECTED = new int[]{2, 1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 24, 23, 22};

    private static final int[] PADED_2D_ZERO = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 0, 0, 0, 0, 0, 5, 6, 7, 8, 0, 0, 0, 0, 0, 9, 10, 11, 12, 0, 0, 0, 0, 0, 13, 14, 15, 16, 0, 0, 0, 0, 0, 17, 18, 19, 20, 0, 0, 0, 0, 0, 21, 22, 23, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private static final int[] PADED_2D_FIXED = new int[]{1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 5, 5, 5, 6, 7, 8, 8, 8, 8, 9, 9, 9, 10, 11, 12, 12, 12, 12, 13, 13, 13, 14, 15, 16, 16, 16, 16, 17, 17, 17, 18, 19, 20, 20, 20, 20, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24};
    private static final int[] PADED_2D_PERIODIC = new int[]{19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 15, 16, 13, 14, 15, 16, 13, 14, 15, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11};
    private static final int[] PADED_2D_REFLECTED = new int[]{6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 2, 1, 1, 2, 3, 4, 4, 3, 2, 6, 5, 5, 6, 7, 8, 8, 7, 6, 10, 9, 9, 10, 11, 12, 12, 11, 10, 14, 13, 13, 14, 15, 16, 16, 15, 14, 18, 17, 17, 18, 19, 20, 20, 19, 18, 22, 21, 21, 22, 23, 24, 24, 23, 22, 22, 21, 21, 22, 23, 24, 24, 23, 22, 18, 17, 17, 18, 19, 20, 20, 19, 18, 14, 13, 13, 14, 15, 16, 16, 15, 14};

    private static final int[] PADED_3D_ZERO = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 0, 0, 0, 0, 0, 5, 6, 7, 8, 0, 0, 0, 0, 0, 9, 10, 11, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 14, 15, 16, 0, 0, 0, 0, 0, 17, 18, 19, 20, 0, 0, 0, 0, 0, 21, 22, 23, 24, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private static final int[] PADED_3D_FIXED = new int[]{1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 5, 5, 5, 6, 7, 8, 8, 8, 8, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 5, 5, 5, 6, 7, 8, 8, 8, 8, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 1, 1, 1, 2, 3, 4, 4, 4, 4, 5, 5, 5, 6, 7, 8, 8, 8, 8, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 9, 9, 9, 10, 11, 12, 12, 12, 12, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 17, 17, 17, 18, 19, 20, 20, 20, 20, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 17, 17, 17, 18, 19, 20, 20, 20, 20, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 17, 17, 17, 18, 19, 20, 20, 20, 20, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 13, 13, 13, 14, 15, 16, 16, 16, 16, 17, 17, 17, 18, 19, 20, 20, 20, 20, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24, 21, 21, 21, 22, 23, 24, 24, 24, 24};
    private static final int[] PADED_3D_PERIODIC = new int[]{7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 15, 16, 13, 14, 15, 16, 13, 14, 15, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 15, 16, 13, 14, 15, 16, 13, 14, 15, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 15, 16, 13, 14, 15, 16, 13, 14, 15, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 15, 16, 13, 14, 15, 16, 13, 14, 15, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 15, 16, 13, 14, 15, 16, 13, 14, 15, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 15, 16, 13, 14, 15, 16, 13, 14, 15, 19, 20, 17, 18, 19, 20, 17, 18, 19, 23, 24, 21, 22, 23, 24, 21, 22, 23, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11, 3, 4, 1, 2, 3, 4, 1, 2, 3, 7, 8, 5, 6, 7, 8, 5, 6, 7, 11, 12, 9, 10, 11, 12, 9, 10, 11};
    private static final int[] PADED_3D_REFLECTED = new int[]{18, 17, 17, 18, 19, 20, 20, 19, 18, 14, 13, 13, 14, 15, 16, 16, 15, 14, 14, 13, 13, 14, 15, 16, 16, 15, 14, 18, 17, 17, 18, 19, 20, 20, 19, 18, 22, 21, 21, 22, 23, 24, 24, 23, 22, 22, 21, 21, 22, 23, 24, 24, 23, 22, 18, 17, 17, 18, 19, 20, 20, 19, 18, 14, 13, 13, 14, 15, 16, 16, 15, 14, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 2, 1, 1, 2, 3, 4, 4, 3, 2, 6, 5, 5, 6, 7, 8, 8, 7, 6, 10, 9, 9, 10, 11, 12, 12, 11, 10, 10, 9, 9, 10, 11, 12, 12, 11, 10, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 2, 1, 1, 2, 3, 4, 4, 3, 2, 6, 5, 5, 6, 7, 8, 8, 7, 6, 10, 9, 9, 10, 11, 12, 12, 11, 10, 10, 9, 9, 10, 11, 12, 12, 11, 10, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 18, 17, 17, 18, 19, 20, 20, 19, 18, 14, 13, 13, 14, 15, 16, 16, 15, 14, 14, 13, 13, 14, 15, 16, 16, 15, 14, 18, 17, 17, 18, 19, 20, 20, 19, 18, 22, 21, 21, 22, 23, 24, 24, 23, 22, 22, 21, 21, 22, 23, 24, 24, 23, 22, 18, 17, 17, 18, 19, 20, 20, 19, 18, 14, 13, 13, 14, 15, 16, 16, 15, 14, 18, 17, 17, 18, 19, 20, 20, 19, 18, 14, 13, 13, 14, 15, 16, 16, 15, 14, 14, 13, 13, 14, 15, 16, 16, 15, 14, 18, 17, 17, 18, 19, 20, 20, 19, 18, 22, 21, 21, 22, 23, 24, 24, 23, 22, 22, 21, 21, 22, 23, 24, 24, 23, 22, 18, 17, 17, 18, 19, 20, 20, 19, 18, 14, 13, 13, 14, 15, 16, 16, 15, 14, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 2, 1, 1, 2, 3, 4, 4, 3, 2, 6, 5, 5, 6, 7, 8, 8, 7, 6, 10, 9, 9, 10, 11, 12, 12, 11, 10, 10, 9, 9, 10, 11, 12, 12, 11, 10, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2, 2, 1, 1, 2, 3, 4, 4, 3, 2, 6, 5, 5, 6, 7, 8, 8, 7, 6, 10, 9, 9, 10, 11, 12, 12, 11, 10, 10, 9, 9, 10, 11, 12, 12, 11, 10, 6, 5, 5, 6, 7, 8, 8, 7, 6, 2, 1, 1, 2, 3, 4, 4, 3, 2};

    private static final long[] DIMS_1D = new long[]{24};
    private static final long[] DIMS_2D = new long[]{4, 6};
    private static final long[] DIMS_3D = new long[]{4, 3, 2};

    private static final long[][] PADDING_SIZE_1D = new long[][]{{2, 3}};
    private static final long[][] PADDING_SIZE_2D = new long[][]{{2, 3}, {2, 3}};
    private static final long[][] PADDING_SIZE_3D = new long[][]{{2, 3}, {2, 3}, {2, 3}};

    private static final ArrayList<Float> TIME_SERIES = new ArrayList<>(Arrays.asList(1.2f, 3.4f));
    private static final double CONSTANT_VALUE = 1.5;

    private static RegularField field1D;
    private static RegularField field2D;
    private static RegularField field3D;
    private static long[] outDims1D;
    private static long[] outDims2D;
    private static long[] outDims3D;

    public PaddingTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
        outDims1D = new long[]{DIMS_1D[0] + PADDING_SIZE_1D[0][0] + PADDING_SIZE_1D[0][1]};
        outDims2D = new long[]{DIMS_2D[0] + PADDING_SIZE_2D[0][0] + PADDING_SIZE_2D[0][1], DIMS_2D[1] + PADDING_SIZE_2D[1][0] + PADDING_SIZE_2D[1][1]};
        outDims3D = new long[]{DIMS_3D[0] + PADDING_SIZE_3D[0][0] + PADDING_SIZE_3D[0][1], DIMS_3D[1] + PADDING_SIZE_3D[1][0] + PADDING_SIZE_3D[1][1], DIMS_3D[2] + PADDING_SIZE_3D[2][0] + PADDING_SIZE_3D[2][1]};
        ArrayList<LargeArray> dataSeries = new ArrayList<>(Arrays.asList(new IntLargeArray(DATA), new IntLargeArray(DATA)));
        field1D = new RegularField(DIMS_1D);
        field1D.addComponent(DataArray.create(DATA, 1, "data1"));
        field1D.addComponent(DataArray.create(new TimeData(TIME_SERIES, dataSeries, TIME_SERIES.get(0)), 1, "data2"));
        field1D.addComponent(DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, DATA.length, CONSTANT_VALUE, "data3"));
        field2D = new RegularField(DIMS_2D);
        field2D.addComponent(DataArray.create(DATA, 1, "data1"));
        field2D.addComponent(DataArray.create(new TimeData(TIME_SERIES, dataSeries, TIME_SERIES.get(0)), 1, "data2"));
        field2D.addComponent(DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, DATA.length, CONSTANT_VALUE, "data3"));
        field3D = new RegularField(DIMS_3D);
        field3D.addComponent(DataArray.create(DATA, 1, "data1"));
        field3D.addComponent(DataArray.create(new TimeData(TIME_SERIES, dataSeries, TIME_SERIES.get(0)), 1, "data2"));
        field3D.addComponent(DataArray.createConstant(DataArrayType.FIELD_DATA_DOUBLE, DATA.length, CONSTANT_VALUE, "data3"));
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of padField method, of class Padding.
     */
    @Test
    public void testPadField1D()
    {
        //ZERO
        RegularField result = Padding.padField(field1D, new int[]{0}, PADDING_SIZE_1D, Padding.PaddingType.ZERO, false);
        assertNotNull(result);
        assertArrayEquals(outDims1D, result.getLDims());
        assertArrayEquals(PADED_1D_ZERO, result.getComponent(0).getRawArray().getIntData());

        //FIXED
        result = Padding.padField(field1D, new int[]{0}, PADDING_SIZE_1D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(outDims1D, result.getLDims());
        assertArrayEquals(PADED_1D_FIXED, result.getComponent(0).getRawArray().getIntData());

        //PERIODIC
        result = Padding.padField(field1D, new int[]{0}, PADDING_SIZE_1D, Padding.PaddingType.PERIODIC, false);
        assertNotNull(result);
        assertArrayEquals(outDims1D, result.getLDims());
        assertArrayEquals(PADED_1D_PERIODIC, result.getComponent(0).getRawArray().getIntData());

        //REFLECTED
        result = Padding.padField(field1D, new int[]{0}, PADDING_SIZE_1D, Padding.PaddingType.REFLECTED, false);
        assertNotNull(result);
        assertArrayEquals(outDims1D, result.getLDims());
        assertArrayEquals(PADED_1D_REFLECTED, result.getComponent(0).getRawArray().getIntData());

        //FIXED padding of all components
        result = Padding.padField(field1D, null, PADDING_SIZE_1D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(outDims1D, result.getLDims());
        assertArrayEquals(PADED_1D_FIXED, result.getComponent(0).getRawArray().getIntData());
        assertArrayEquals(PADED_1D_FIXED, result.getComponent(1).getTimeData().getValues().get(0).getIntData());
        assertArrayEquals(PADED_1D_FIXED, result.getComponent(1).getTimeData().getValues().get(1).getIntData());
        assertEquals(TIME_SERIES, result.getComponent(1).getTimeSeries());
        assertTrue(result.getComponent(2).isConstant());
        FloatingPointAsserts.assertRelativeEquals(CONSTANT_VALUE, result.getComponent(2).getDoubleElement(0)[0]);
    }

    /**
     * Test of padField method, of class Padding.
     */
    @Test
    public void testPadField2D()
    {
        //ZERO
        RegularField result = Padding.padField(field2D, new int[]{0}, PADDING_SIZE_2D, Padding.PaddingType.ZERO, false);
        assertNotNull(result);
        assertArrayEquals(outDims2D, result.getLDims());
        assertArrayEquals(PADED_2D_ZERO, result.getComponent(0).getRawArray().getIntData());

        //FIXED
        result = Padding.padField(field2D, new int[]{0}, PADDING_SIZE_2D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(outDims2D, result.getLDims());
        assertArrayEquals(PADED_2D_FIXED, result.getComponent(0).getRawArray().getIntData());

        //PERIODIC
        result = Padding.padField(field2D, new int[]{0}, PADDING_SIZE_2D, Padding.PaddingType.PERIODIC, false);
        assertNotNull(result);
        assertArrayEquals(outDims2D, result.getLDims());
        assertArrayEquals(PADED_2D_PERIODIC, result.getComponent(0).getRawArray().getIntData());

        //REFLECTED
        result = Padding.padField(field2D, new int[]{0}, PADDING_SIZE_2D, Padding.PaddingType.REFLECTED, false);
        assertNotNull(result);
        assertArrayEquals(outDims2D, result.getLDims());
        assertArrayEquals(PADED_2D_REFLECTED, result.getComponent(0).getRawArray().getIntData());

        //FIXED padding of all components
        result = Padding.padField(field2D, null, PADDING_SIZE_2D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(outDims2D, result.getLDims());
        assertArrayEquals(PADED_2D_FIXED, result.getComponent(0).getRawArray().getIntData());
        assertArrayEquals(PADED_2D_FIXED, result.getComponent(1).getTimeData().getValues().get(0).getIntData());
        assertArrayEquals(PADED_2D_FIXED, result.getComponent(1).getTimeData().getValues().get(1).getIntData());
        assertEquals(TIME_SERIES, result.getComponent(1).getTimeSeries());
        assertTrue(result.getComponent(2).isConstant());
        FloatingPointAsserts.assertRelativeEquals(CONSTANT_VALUE, result.getComponent(2).getDoubleElement(0)[0]);
    }

    /**
     * Test of padField method, of class Padding.
     */
    @Test
    public void testPadField3D()
    {
        //ZERO
        RegularField result = Padding.padField(field3D, new int[]{0}, PADDING_SIZE_3D, Padding.PaddingType.ZERO, false);
        assertNotNull(result);
        assertArrayEquals(outDims3D, result.getLDims());
        assertArrayEquals(PADED_3D_ZERO, result.getComponent(0).getRawArray().getIntData());

        //FIXED
        result = Padding.padField(field3D, new int[]{0}, PADDING_SIZE_3D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(outDims3D, result.getLDims());
        assertArrayEquals(PADED_3D_FIXED, result.getComponent(0).getRawArray().getIntData());

        //PERIODIC
        result = Padding.padField(field3D, new int[]{0}, PADDING_SIZE_3D, Padding.PaddingType.PERIODIC, false);
        assertNotNull(result);
        assertArrayEquals(outDims3D, result.getLDims());
        assertArrayEquals(PADED_3D_PERIODIC, result.getComponent(0).getRawArray().getIntData());

        //REFLECTED
        result = Padding.padField(field3D, new int[]{0}, PADDING_SIZE_3D, Padding.PaddingType.REFLECTED, false);
        assertNotNull(result);
        assertArrayEquals(outDims3D, result.getLDims());
        assertArrayEquals(PADED_3D_REFLECTED, result.getComponent(0).getRawArray().getIntData());

        //FIXED padding of all components
        result = Padding.padField(field3D, null, PADDING_SIZE_3D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(outDims3D, result.getLDims());
        assertArrayEquals(PADED_3D_FIXED, result.getComponent(0).getRawArray().getIntData());
        assertArrayEquals(PADED_3D_FIXED, result.getComponent(1).getTimeData().getValues().get(0).getIntData());
        assertArrayEquals(PADED_3D_FIXED, result.getComponent(1).getTimeData().getValues().get(1).getIntData());
        assertEquals(TIME_SERIES, result.getComponent(1).getTimeSeries());
        assertTrue(result.getComponent(2).isConstant());
        FloatingPointAsserts.assertRelativeEquals(CONSTANT_VALUE, result.getComponent(2).getDoubleElement(0)[0]);
    }

    /**
     * Test of padDataArray method, of class Padding.
     */
    @Test
    public void testPadDataArray1D()
    {
        //ZERO
        DataArray result = Padding.padDataArray(field1D.getComponent(0), field1D.getLDims(), PADDING_SIZE_1D, Padding.PaddingType.ZERO, false);
        assertNotNull(result);
        assertArrayEquals(PADED_1D_ZERO, result.getRawArray().getIntData());

        //FIXED
        result = Padding.padDataArray(field1D.getComponent(0), field1D.getLDims(), PADDING_SIZE_1D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(PADED_1D_FIXED, result.getRawArray().getIntData());

        //PERIODIC
        result = Padding.padDataArray(field1D.getComponent(0), field1D.getLDims(), PADDING_SIZE_1D, Padding.PaddingType.PERIODIC, false);
        assertNotNull(result);
        assertArrayEquals(PADED_1D_PERIODIC, result.getRawArray().getIntData());

        //REFLECTED
        result = Padding.padDataArray(field1D.getComponent(0), field1D.getLDims(), PADDING_SIZE_1D, Padding.PaddingType.REFLECTED, false);
        assertNotNull(result);
        assertArrayEquals(PADED_1D_REFLECTED, result.getRawArray().getIntData());
    }

    /**
     * Test of padDataArray method, of class Padding.
     */
    @Test
    public void testPadDataArray2D()
    {
        //ZERO
        DataArray result = Padding.padDataArray(field2D.getComponent(0), field2D.getLDims(), PADDING_SIZE_2D, Padding.PaddingType.ZERO, false);
        assertNotNull(result);
        assertArrayEquals(PADED_2D_ZERO, result.getRawArray().getIntData());

        //FIXED
        result = Padding.padDataArray(field2D.getComponent(0), field2D.getLDims(), PADDING_SIZE_2D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(PADED_2D_FIXED, result.getRawArray().getIntData());

        //PERIODIC
        result = Padding.padDataArray(field2D.getComponent(0), field2D.getLDims(), PADDING_SIZE_2D, Padding.PaddingType.PERIODIC, false);
        assertNotNull(result);
        assertArrayEquals(PADED_2D_PERIODIC, result.getRawArray().getIntData());

        //REFLECTED
        result = Padding.padDataArray(field2D.getComponent(0), field2D.getLDims(), PADDING_SIZE_2D, Padding.PaddingType.REFLECTED, false);
        assertNotNull(result);
        assertArrayEquals(PADED_2D_REFLECTED, result.getRawArray().getIntData());
    }

    /**
     * Test of padDataArray method, of class Padding.
     */
    @Test
    public void testPadDataArray3D()
    {
        //ZERO
        DataArray result = Padding.padDataArray(field3D.getComponent(0), field3D.getLDims(), PADDING_SIZE_3D, Padding.PaddingType.ZERO, false);
        assertNotNull(result);
        assertArrayEquals(PADED_3D_ZERO, result.getRawArray().getIntData());

        //FIXED
        result = Padding.padDataArray(field3D.getComponent(0), field3D.getLDims(), PADDING_SIZE_3D, Padding.PaddingType.FIXED, false);
        assertNotNull(result);
        assertArrayEquals(PADED_3D_FIXED, result.getRawArray().getIntData());

        //PERIODIC
        result = Padding.padDataArray(field3D.getComponent(0), field3D.getLDims(), PADDING_SIZE_3D, Padding.PaddingType.PERIODIC, false);
        assertNotNull(result);
        assertArrayEquals(PADED_3D_PERIODIC, result.getRawArray().getIntData());

        //REFLECTED
        result = Padding.padDataArray(field3D.getComponent(0), field3D.getLDims(), PADDING_SIZE_3D, Padding.PaddingType.REFLECTED, false);
        assertNotNull(result);
        assertArrayEquals(PADED_3D_REFLECTED, result.getRawArray().getIntData());
    }

}
